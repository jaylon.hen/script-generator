import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  columns;
  lines;
  sqlText: string;
  fileName: string;
  generatedSQL: string;
  options: any = {maxLines: 1000, printMargin: false};
  
  @ViewChild('file') file;
  @ViewChild('uploader') uploader;
  @ViewChild('sqlEditor') sqlEditor;
  @ViewChild('formattedSQL') formattedSQL;
  @ViewChild('uploadResult') uploadResult;

  readFile(files) {
    if (files.length) {
      var r = new FileReader();
      const self = this;

      r.onload = function(e) {
          let contents =  <string> e.target.result;
          console.log(e);
          self.lines = contents.split('\n');
          self.columns = self.lines.shift().split('\t');
          self.uploader.nativeElement.classList.add('hidden');
          self.uploadResult.nativeElement.classList.add('shown');
      };
      
      r.readAsText(files[0]);
      this.fileName = files[0].name;
    }
  }

  getFirstName(fullName) {
    return fullName.split(' ')[0];
  }

  getLastName(fullName) {
    return fullName.split(' ').pop();
  }

  addVariable(variable) {
    variable = ':' + variable.trim() + ':';

    this.sqlEditor.getEditor().insert(variable);
    this.sqlEditor.getEditor().focus();
  }

  generateScript() {
    let rawSQL = this.sqlEditor.getEditor().getValue();
    this.generatedSQL = '';
    

    this.lines.forEach(line => {
      let formattedSQL = rawSQL;

      this.columns.forEach((column,index) => {
        let values = line.split('\t');
        formattedSQL = formattedSQL.split(':' + column.trim() + ':').join(values[index].trim());
      });

      this.generatedSQL += formattedSQL;
      this.generatedSQL += '\n';
    });
  }

  copyText() {
    const el = document.createElement('textarea');
    el.value = this.formattedSQL.getEditor().getValue();
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    
    const selected =
      document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    if (selected) {
      document.getSelection().removeAllRanges();
      document.getSelection().addRange(selected);
    }

    document.execCommand("copy");
  }

  removeFile() {
    this.file.nativeElement.value = "";
    this.uploader.nativeElement.classList.remove('hidden');
    this.uploadResult.nativeElement.classList.remove('shown');
    this.sqlEditor.getEditor().setValue('');
    this.formattedSQL.getEditor().setValue('');
    this.columns = null;
    this.lines = null;
  }
}
